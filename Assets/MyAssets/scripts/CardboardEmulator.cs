﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardboardEmulator : MonoBehaviour
{
    //Emulation: alt move mouse: yaw (rond y-as) / pitch (rond x-as)
    //           ctrl move mouse: roll (rond z-as)
    private const float HEAD_MOVEMENT_FORCE = 500;

    [SerializeField]
    private Camera camera;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftAlt))
        {
            Vector3 rotation = new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0);
            camera.transform.Rotate(rotation * HEAD_MOVEMENT_FORCE * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.LeftControl))
        {
            Vector3 rotation = new Vector3(0, 0, Input.GetAxis("Mouse X"));
            camera.transform.Rotate(rotation * HEAD_MOVEMENT_FORCE * Time.deltaTime);
        }
        else
        {
            camera.transform.eulerAngles = new Vector3(camera.transform.eulerAngles.x, camera.transform.eulerAngles.y, 0);
        }
    }
}
