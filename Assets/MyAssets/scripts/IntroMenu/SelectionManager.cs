﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SelectionManager : MonoBehaviour
{

    private float rayDistance = 1500;
    private GameObject lookAtButton;
    public LayerMask layerMask;
    private TMP_Text text;
    [SerializeField]
    private TMP_ColorGradient green;
    [SerializeField]
    private TMP_ColorGradient blue;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, rayDistance, layerMask))
        {

            lookAtButton = hit.transform.gameObject;
            text = (TMP_Text)lookAtButton.GetComponentInChildren<TMP_Text>();
            text.colorGradientPreset = green;


            if (Input.GetKeyDown(KeyCode.Space) || Google.XR.Cardboard.Api.IsTriggerPressed)
            {
                 lookAtButton.GetComponent<Button>().onClick.Invoke();
            }
        }
        else
        {
            if (text != null)
            {
                text.colorGradientPreset = blue;
            }

        }

    }
}
