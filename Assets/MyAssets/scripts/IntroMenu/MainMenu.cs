﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject gravityMenu;
    [SerializeField]
    private GameObject infoMenu;
   // [SerializeField]
    //private Camera cam;
    private GameObject lookAtButton;
    private const float rayDistance = 1500;
    [SerializeField]
    private LayerMask layerMask;


    void Start()
    {
       
    }

    public void PlayGame()
    {
        SceneManager.LoadScene("MainScene");
    }
}
