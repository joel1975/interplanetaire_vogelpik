﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField]
    private GameObject rocket;
    private Vector3 distance;

    // Start is called before the first frame update
    void Start()
    {
        distance = this.transform.position - rocket.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        if (rocket != null)
        {
            this.transform.position = rocket.transform.position + distance;
        }
      
    }
}
