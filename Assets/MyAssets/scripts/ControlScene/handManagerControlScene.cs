﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class handManagerControlScene : MonoBehaviour
{

    [SerializeField]
    private TMP_Text controls;
    private Animator animator;
    private ParticleSystem exhaust;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        exhaust = GameObject.Find("Exhaust").GetComponent<ParticleSystem>();
        exhaust.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void showUpText()
    {
        controls.text = "Look Up";
    }

    void showDownText()
    {
        controls.text = "Look Down";
    }

    void showLeftText()
    {
        controls.text = "Look Left";
    }

    void showRightText()
    {
        controls.text = "Look Right";
    }

    void clearText()
    {
        controls.text = "";
    }

    void thrustText()
    {
        controls.text = "Look at thrust";
        exhaust.Play();
    }

    void EndScene()
    {
        SceneManager.LoadScene("worldspaceIntroScene");
    }
}
