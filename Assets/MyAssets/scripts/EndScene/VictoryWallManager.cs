﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;



public class VictoryWallManager : MonoBehaviour
{

    [SerializeField]
    private TMP_Text descripton;
    [SerializeField]
    private TMP_Text fuelConsumption;
    [SerializeField]
    private TMP_Text ringPoints;


    // Start is called before the first frame update
    void Start()
    {
        descripton.text = PlayerPrefs.GetString("result");
        fuelConsumption.text = "Fuel Consumption: " + (100 - PlayerPrefs.GetInt("fuel")) + "%";
        int rings = PlayerPrefs.GetInt("points");
        ringPoints.text = rings == 3 ? "Great, you've gone through all the rings!" : "You missed " + (3 - rings) + (3 - rings == 1 ? " ring" : " rings" );
        PlayerPrefs.SetInt("points", 0);
        TracjetRocket.traject = new List<Vector3>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setIntroScene()
    {
        SceneManager.LoadScene("worldspaceIntroScene");
    }
}
