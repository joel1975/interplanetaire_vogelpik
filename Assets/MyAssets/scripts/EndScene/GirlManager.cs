﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class GirlManager : MonoBehaviour
{

    private Animator animator;
    [SerializeField]
    private Button bckBtn;
    [SerializeField]
    private Camera cam;
    private NavMeshAgent girl;
   

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent < Animator>();
        girl = GetComponent<NavMeshAgent>();
        setAnimation();
        bckBtn.onClick.AddListener(WalkOnClick);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void setAnimation()
    {
       
        int points = PlayerPrefs.GetInt("points");
        string result = PlayerPrefs.GetString("result");


        if (result.Equals("You crashed on the Moon!!") || result.Equals("You crashed on Earth!!")) {

            animator.SetTrigger("isCrashed");
        }
        else if (points == 3) 
        {
            animator.SetTrigger("isWon");
        }
        else
        {
            animator.SetTrigger("isLost");
        }  
    }

    void WalkOnClick()
    {
        animator.SetTrigger("isBckBtnClicked");
        girl.SetDestination(bckBtn.transform.position);
    }

    

    private void OnTriggerEnter(Collider other)
    {
        animator.SetTrigger("isAtBckBtn");
    }

    public void pushBackBtn()
    {
        SceneManager.LoadScene("worldspaceIntroScene");
    }
}
