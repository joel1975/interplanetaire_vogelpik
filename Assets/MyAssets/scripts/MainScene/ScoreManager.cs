﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class ScoreManager : MonoBehaviour
{

    [SerializeField]
    private TMP_Text fuel;
    

    // Start is called before the first frame update
    void Start()
    {
        fuel.text = PlayerPrefs.GetInt("fuel").ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
