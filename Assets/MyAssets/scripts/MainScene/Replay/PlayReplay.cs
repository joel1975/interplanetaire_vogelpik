﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayReplay : MonoBehaviour
{
    [SerializeField]
    private TMP_Text rplText;
    private int counter = 0;
    private float replayTimer = 0;
    
    
    // Start is called before the first frame update
    void Start()
    {
     
       

    }

    // Update is called once per frame
    void Update()
    {
        replayTimer += Time.deltaTime;
        
        if ((int)replayTimer % 2 == 0)
        {
            rplText.text = "";
        } else
        {
            rplText.text = "REPLAY";
        }
    }

    private void FixedUpdate()
    {
        transform.position = TracjetRocket.traject[counter];
      
        if (counter != TracjetRocket.traject.Count -1)
        {
            counter++;
        } else
        {
            SceneManager.LoadScene("EndScene");
        }



    }
}
