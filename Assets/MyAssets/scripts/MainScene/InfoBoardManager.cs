﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InfoBoardManager : MonoBehaviour
{

    [SerializeField]
    private Text fuelText;
    [SerializeField]
    private Image fuelImage;
    [SerializeField]
    private TMP_Text earthGravityText;
    [SerializeField]
    private TMP_Text moonGravityText;
    [SerializeField]
    private TMP_Text earthDistance;
    [SerializeField]
    private TMP_Text moonDistance;
    [SerializeField]
    private TMP_Text scoreText;
    private int points = 0;
   

    public void setFuelAmount(int amount)
    {
        fuelText.text = "Fuel: " + amount + "%";
    }

    public void setFuelImage(float amount)
    {
        fuelImage.fillAmount = amount;
    }

    public void setEarthGravetiy(float amount)
    {
        earthGravityText.text = "F = " + amount.ToString("####.##") + "N";
    }

    public void setMoonGravetiy(float amount)
    {
        moonGravityText.text = "F = " + amount.ToString("####.#") + "N";
    }

    public void setEarthDistance(float distance)
    {
        earthDistance.text = "Distance: " + distance.ToString("#####") + "km";
    }

    public void setMoonDistance(float distance)
    {
        moonDistance.text = "Distance: " + distance.ToString("#####") + "km";
    }

    public void addPoint()
    {
        points++;
        scoreText.text = points + " / 3";
        PlayerPrefs.SetInt("points", points);

    }

    public void setEndFuel(int amount)
    {
        PlayerPrefs.SetInt("fuel", amount);
    }
   
}
