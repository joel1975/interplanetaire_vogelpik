﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class EarthManager : MonoBehaviour
{
    private AudioSource audioSource;
    private GameObject rocket;
    private float distance;
    private InfoBoardManager infoBoardManager;
    private bool isCrashed = false;
    private float delay = 5f;
   

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        rocket = GameObject.FindGameObjectWithTag("Rocket");
        distance = Vector3.Distance(this.transform.position, rocket.transform.position);
        infoBoardManager = GameObject.FindGameObjectWithTag("InfoBoard").GetComponent<InfoBoardManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rocket != null)
        {
            distance = Vector3.Distance(this.transform.position, rocket.transform.position);
            infoBoardManager.setEarthDistance(distance);
        }

        if (isCrashed)
        {
            if (delay < 0)
            {
                Debug.Log("EarthManager crash : go to replay");
                SceneManager.LoadScene("ReplayScene");
            }
            else
            {
                delay -= Time.deltaTime;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            audioSource.Play();
            Destroy(other.gameObject);
            PlayerPrefs.SetString("result", "You crashed on Earth!!");
            isCrashed = true;
        }
    }
}
