﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RingManager : MonoBehaviour
{

    private Renderer rnd;
    private AudioSource okey;
    private InfoBoardManager infoBoardManager;

    // Start is called before the first frame update
    void Start()
    {
        rnd = GetComponent<Renderer>();
        okey = GetComponent<AudioSource>();
        infoBoardManager = GameObject.FindGameObjectWithTag("InfoBoard").GetComponent<InfoBoardManager>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            rnd.material.color = Color.green;
            okey.Play();
            infoBoardManager.addPoint();
        }
    }
}
