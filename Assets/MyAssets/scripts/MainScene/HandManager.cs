﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandManager : MonoBehaviour
{

    public bool isAnimationFinished;
    private Animator animator;
    private float rotationForce = 10;
    private RocketManager rocketManager;
    public bool isHandOpen = false;
   
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        animator.Play("squeezeRocket");
        isAnimationFinished = false;
        rocketManager = GameObject.FindGameObjectWithTag("Rocket").GetComponent<RocketManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isAnimationFinished && rocketManager.isFirstTrust)
        {
            RotateLeftRight();
            RotateUpDown();
        }

        if (rocketManager.isThrustFinished)
        {
            animator.SetTrigger("letGo");
        }

    }
    public void setAnimationFinished()
    {
        isAnimationFinished = true;
    }

    private void RotateLeftRight()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.forward * rotationForce * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(-Vector3.forward * rotationForce * Time.deltaTime);
        }

    }

    private void RotateUpDown()
    {
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Rotate(Vector3.up * rotationForce * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Rotate(-Vector3.up * rotationForce * Time.deltaTime);
        }
    }

    public void setOpenHand()
    {
        isHandOpen = true;
    }


}
