﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ControlsManager : MonoBehaviour
{

    private float rayDistance = 800;
    private GameObject lookAtButton;
    public LayerMask layerMask;
    private TMP_Text text;
    [SerializeField]
    private TMP_ColorGradient green;
    [SerializeField]
    private TMP_ColorGradient blue;
    private GameObject hand;
    private float rotationForce = 5;
    public bool isThrust = false;
    public bool isFirstTimeThrust = true;

    // Start is called before the first frame update
    void Start()
    {
        hand = GameObject.FindGameObjectWithTag("Hand");
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
 
        if (Physics.Raycast(transform.position, transform.forward, out hit, rayDistance, layerMask))
        {
            lookAtButton = hit.transform.gameObject;
            text = (TMP_Text)lookAtButton.GetComponentInChildren<TMP_Text>();
            text.colorGradientPreset = green;

            if (hand != null)
            {
                switch (lookAtButton.name)
                {
                    case "BtnU":
                        hand.transform.Rotate(-Vector3.up * rotationForce * Time.deltaTime);
                        break;
                    case "BtnD":
                        hand.transform.Rotate(Vector3.up * rotationForce * Time.deltaTime);
                        break;
                    case "BtnL":
                        hand.transform.Rotate(Vector3.forward * rotationForce * Time.deltaTime);
                        break;
                    case "BtnR":
                        hand.transform.Rotate(-Vector3.forward * rotationForce * Time.deltaTime);
                        break;
                }
            }

            if (lookAtButton.name.Equals("BtnT"))
            {
                isThrust = true;
                isFirstTimeThrust = false;
            }

        } else
        {
            if (text != null)
            {
                text.colorGradientPreset = blue;
            }

            if (!isFirstTimeThrust)
            {
                isThrust = false;
            }
        }
    }
}
