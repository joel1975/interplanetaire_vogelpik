﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RocketManager : MonoBehaviour
{
    //[SerializeField]
    private float thrustForce = 170;
    private ParticleSystem exhaust;
    private ParticleSystem frontOfRocket;
    public bool isThrustFinished = false;
    private Rigidbody rgdBody;
    private AudioSource audioSource;
    private float THRUST_MAX_TIME = 5;
    private float thrustCurrentTime = 0;
    private bool isThrustKeyDown;
    public bool isFirstTrust = true;
 
    private Rigidbody earthRgdBody;
    private Rigidbody moonRgdBody;
    private float gravity = 13000.0f;
    private HandManager handManager;
    private InfoBoardManager infoBoardManager;
    private ControlsManager controlsManager;
   
    
   

    // Start is called before the first frame update
    void Start()
    {
        rgdBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        exhaust = GameObject.Find("Exhaust").GetComponent<ParticleSystem>();
        frontOfRocket = GameObject.Find("Front").GetComponent<ParticleSystem>();
        exhaust.Stop();
        earthRgdBody = GameObject.FindGameObjectWithTag("Earth").GetComponent<Rigidbody>();
        moonRgdBody = GameObject.FindGameObjectWithTag("Moon").GetComponent<Rigidbody>();
        frontOfRocket.Stop();
        infoBoardManager = GameObject.FindGameObjectWithTag("InfoBoard").GetComponent<InfoBoardManager>();
        handManager = GameObject.FindGameObjectWithTag("Hand").GetComponent<HandManager>();
        controlsManager = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<ControlsManager>();
    }

   

    // Update is called once per frame
    void Update()
    {

        if (isFirstTrust && handManager.isAnimationFinished)
        {
            Thrust();
        } 
    
        if (!isFirstTrust)
        {
            frontOfRocket.Play();
        }

        if (transform.position.z > 7000)
        {
            PlayerPrefs.SetString("result", "Great, you didn't crashed");
            SceneManager.LoadScene("ReplayScene");
        }
    }

    private void FixedUpdate()
    {
        
        if (isThrustKeyDown && isFirstTrust)
        {
            thrustCurrentTime += Time.deltaTime;
            UpdateUI();
      
            if (thrustCurrentTime > THRUST_MAX_TIME)
            {
                isThrustFinished = true;
                isFirstTrust = false;
                stopAudioAndFlames();
            }
        }

        if (isThrustFinished && handManager.isHandOpen)
        {
            rgdBody.AddRelativeForce(Vector3.up * thrustForce * thrustCurrentTime, ForceMode.VelocityChange);
            infoBoardManager.setEndFuel((int)(100 - (thrustCurrentTime * 20)));
            isThrustFinished = false;
        }

        if (!isFirstTrust)
        {
            TracjetRocket.traject.Add(transform.position);
            Attrack(earthRgdBody);
            Attrack(moonRgdBody);
        } 
    }

    private void Thrust()
    {

        if (Input.GetButton("Thrust") || controlsManager.isThrust)
        {
            if (!audioSource.isPlaying)
            {
                isThrustKeyDown = true;
                startAudioAndFlames();
            }
        }
        else if (Input.GetButtonUp("Thrust") || (!(controlsManager.isThrust ||  controlsManager.isFirstTimeThrust)))
        {
            isThrustFinished = true;
            isThrustKeyDown = false;
            isFirstTrust = false;
            stopAudioAndFlames();
        }
        else
        {
            stopAudioAndFlames();
        }
    }

    private void UpdateUI()
    {
        infoBoardManager.setFuelImage(1f - (thrustCurrentTime / 5));
        infoBoardManager.setFuelAmount((int)(100 - (thrustCurrentTime * 20)));
    }

    private void stopAudioAndFlames()
    {
        audioSource.Stop();
        exhaust.Stop();
    }

    private void startAudioAndFlames()
    {
        audioSource.Play();
        exhaust.Play();
    }

    private void Attrack(Rigidbody rgb)
    {
    
        Vector3 otherToThis = rgb.position - rgdBody.position;
        Vector3 thisToOther = rgdBody.position - rgb.position;
 

        //float distance = otherToThis.magnitude;
        float distance = otherToThis.sqrMagnitude;   
        // F = G * ( m1 * m2 / ( d^2))
        float forceMagnitude = gravity * ((rgdBody.mass * rgb.mass) / distance);
       
        Vector3 forceToRocket = otherToThis.normalized * forceMagnitude;
        rgdBody.AddForce(forceToRocket);

        Vector3 forceToPlanet = thisToOther.normalized * forceMagnitude;

        if (rgb == earthRgdBody)
        {
            infoBoardManager.setEarthGravetiy(forceMagnitude * 100);
        } else
        {
            infoBoardManager.setMoonGravetiy(forceMagnitude * 100);
        }
        rgb.AddForce(forceToPlanet);
    }

}
