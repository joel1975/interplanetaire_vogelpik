Dag Hans,

Al mijn prefabs (maan, aarde, rocket, rings, arm, skyboxen,...)  komen uit de Unity asset strore.
Enkel het eind character ( en haar animaties) komen uit de Mixamo store.

Het project is gemaakt voor de google cardboard, ik zelf heb een Honor 8 smartphone en hierop werkt het echter niet.
Maar bij het tijdelijk lenen van een andere smartphone (motog8 van motorola) blijkt alles wel te werken.

Besturing:

Intro -en eind scene:

unity ->  kan je gewoon de muis gebruiken en op de button clicken
Carboard ->  moet je met je hoofd bewegen tot de button groen wordt en dan dan op de cardboard trigger clicken 
CardboardEmulater -> spacebar is clicken


Main Scene:

unity -> left, right, up en down arrow key het bewegen van de rocket, eenmaal dat de rocket goed gepositioneerd staat 
         kan je de rocket een "thrust" geven via de spacebar (rechts onder zie je hoe de benzine leeg loopt) 
	 Hoe meer trust ( spacebar inhouden ) hoe sneller de rocket gelanceerd wordt.

Cardboard -> Juist voor de rocket zijn de vier richtingen aangeven via buttons elk in hun eigen richting , je hoeft enkel naar de button te kijken bv rechts (>>) 
             en zolang de button groen kleurt zal de rocket naar rechts bewegen. Weer eenmaal de je denkt dat de rocket goed staat, 
             moet je naar de "thrust" button kijken (links onder) , hiermee geeft je weer "thrust" aan je racket. 
 	     Zolang de thrust button groen blijft des te meer benzine je gebruikt en hoe sneller je rocket gelanceerd wordt.

Dit staat ook uitgelegd in het mainmenu -> Info -> controls

Eenmaal de rocket gelanceerd is kan je niet meer bijsturen of extra snelheid geven!

Mvg

Joël Van Eyken
Informatica blended
 